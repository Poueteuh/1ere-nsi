## Introduction



> Le type booléen, ou `bool`, est un type de données en Python qui a deux valeurs possibles : `True` (vrai) ou `False` (faux). Il est généralement utilisé pour effectuer des comparaisons et des opérations logiques.



## Valeur et Type

En Python, les booléens sont représentés par `True` et `False`, et sont du type `bool`. Par exemple :

```python
>>> True
True
>>> print(type(True))
<class 'bool'>
>>> False
False
>>> print(type(False))
<class 'bool'>
```

------

## Comparaison

Les opérateurs de comparaison en Python sont similaires à ceux utilisés en mathématiques. Il est important de ne pas confondre l'opérateur d'affectation (`=`) avec l'opérateur d'égalité (`==`). Par exemple :

```python
>>> variable = 5    # une affectation
>>> 5 == 8          # une égalité (qui est fausse)
False
```

**Le résultat d'une comparaison est toujours un booléen.**

------

## Comparaisons des nombres

On peut comparer les nombres en utilisant les opérateurs de comparaison suivants :

- Égalité : `==`
- Différence : `!=`
- Supérieur : `>`
- Inférieur : `<`
- Supérieur ou égal : `>=`
- Inférieur ou égal : `<=`

Par exemple :

```python
>>> 1 + 2 == 3
True
>>> 1 + 2 != 3
False
>>> 4 > 3
True
>>> 2.2 < 2 * 3
True
>>> 5 >= 6
False
>>> 8 <= 3
False
```

------

## Appartenance à une structure

On peut utiliser l'opérateur `in` pour vérifier si un élément est présent dans une structure de données. Par exemple :

```python
>>> "a"		in "bonjour"        
False
>>> "bon"	in "bonjour"        
True
>>> 1     in [2, 3, 4]       
False
```

------

## Opérations sur les booléens

Les opérations sur les booléens comprennent la négation (`not`), l'opérateur OU (`or`), l'opérateur ET (`and`) et l'opérateur XOR (`^`). Par exemple :

```python
# La négation
>>> not True
False
>>> not False
True

# L'opérateur OU
>>> False or False
False
>>> False or True
True
>>> True or False
True
>>> True or True
True

# L'opérateur ET
>>> False and False
False
>>> False and True
False
>>> True and False
False
>>> True and True
True

# L'opérateur XOR
>>> False ^ False
False
>>> False ^ True
True
>>> True ^ False
True
>>> True ^ True
False
```

------

## Python et les booléens

Python permet d'évaluer n'importe quel type de donnée comme un booléen. Par exemple :

```python
>>> bool(1)       
True
>>> bool(0)       
False
>>> bool("")      
False
>>> bool("abc")   
True
>>> bool([])     
False
>>> bool([1, 2])  
True
```

En règle générale, zéro, les structures de données vides et `None` sont évalués comme `False`, tandis que les autres valeurs sont évaluées comme `True`.

------

## Complément : `None` et l'identité `is`

Python propose la valeur `None` pour représenter l'absence d'une valeur. On peut utiliser l'opérateur `is` pour vérifier l'identité d'une valeur. Par exemple :

```python
>>> 1 is None
False
>>> "abc" is None
False
>>> None is None
True
>>> a = 5
>>> a is None
False
```

Une fonction qui ne se termine pas par `return ...` renvoie implicitement `None`.

--------

## Opérations bit à bit

Python, comme de nombreux autres langages de programmation, fournit des opérateurs bit à bit pour manipuler les nombres au niveau des bits. 

Ces opérateurs incluent les opérations AND (&), OR (|), XOR (^), NOT (~), ainsi que les décalages de bits à gauche (<<) et à droite (>>).

Voici un exemple de leur utilisation :

```python
a = 10  # 1010 en binaire
b = 4   # 0100 en binaire

print(a & b)  # Opération AND: 0000 en binaire (0 en decimale)
print(a | b)  # Opération OR:  1110 en binaire (14 en decimale)
print(a ^ b)  # Opération XOR: 1110 en binaire (14 en decimale)
print(~a)     # Opération NOT: 0101 en binaire (-11 en decimale, complément à deux)
print(a << 1) # Opération décalage à gauche: 10100 en binaire (20 en decimale)
print(a >> 1) # Opération décalage à droite: 0101 en binaire (5 en decimale)
```



---------



Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.