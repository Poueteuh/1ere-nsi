## **TD : Manipulation des encodages de caractères**

### **Objectifs** :

1. Comprendre comment les caractères sont encodés en utilisant différents systèmes d'encodage.
2. Savoir comment convertir entre différents encodages.
3. Être capable d'identifier l'encodage utilisé pour un texte donné.

### **Partie 1 : Découverte des encodages**

Pour cette partie, utilisez un éditeur de texte qui permet de choisir l'encodage lors de l'enregistrement d'un fichier (comme Notepad++).

1. Créez un nouveau fichier texte, et écrivez le mot "Bonjour" dedans.
2. Enregistrez le fichier en utilisant l'encodage ASCII. Qu'observez-vous ?
3. Réessayez en utilisant l'encodage ISO-8859-1. Qu'est-ce qui change ?
4. Enfin, enregistrez le fichier en utilisant l'encodage UTF-8.

### **Partie 2 : Conversion entre encodages**

Pour cette partie, utilisez un outil en ligne qui permet de convertir entre différents encodages, comme [cet outil](https://www.browserling.com/tools/text-to-binary).

1. Choisissez un mot ou une phrase en français qui utilise des caractères accentués (par exemple, "révolution").
2. Convertissez ce texte en binaire en utilisant l'encodage UTF-8. Notez le résultat.
3. Convertissez maintenant le même texte en binaire en utilisant l'encodage ISO-8859-1. Notez le résultat.
4. Comparez les deux résultats. Qu'observez-vous ?

### **Partie 3 : Identification de l'encodage**

Pour cette partie, utilisez un outil en ligne qui permet de deviner l'encodage d'un texte, comme [cet outil](https://dencode.com).

1. Choisissez trois textes différents, de préférence dans différentes langues.
2. Convertissez chaque texte en binaire en utilisant un encodage différent pour chaque texte.
3. Utilisez l'outil pour essayer de deviner quel encodage a été utilisé pour chaque texte. L'outil a-t-il raison ?

### **Partie 4 : Python**

1. Dans un interpréteur Python, exécutez successivement les instructions `chr(212)` et `chr(169)`. Qu'en déduisez-vous sur la spécification de la fonction `chr`?

```python
>>> chr(212)
'Ô'
>>> chr(169)
'©'
```

2. Dans un interpréteur Python, exécutez successivement les instructions `ord('©')` et `ord('à')`. Qu'en déduisez-vous sur la spécification de la fonction `ord`?

```python
>>> ord('©')
169
>>> ord('à')
224
```

3. Écrire une fonction `binaire_utf8`, qui prend en paramètre un point de code Unicode sous la forme d'un entier et renvoie la représentation binaire UTF-8 du caractère associé, sous la forme d'une liste d'octet (un octet est une liste de bits).

   Conseil : Vous pouvez réutiliser les fonctions de conversion en binaire des précédentes séances ainsi que la méthode vu en cours.

```python
def binaire_utf8(code):
  binaire_code = binaire(code)
  if len(binaire_code) < 8:
    return binaire_code
  elif 8 <= binaire_code < 12:
    return '110' + binaire_code[:5] + '10' + binaire_code[5:]
  elif 12 <= binaire_code < 17:
    return '1110' + binaire_code[:4] + '10' + binaire_code[4:10] + '10' + binaire_code[10:]
  elif 17 <= binaire_code:
    return '11110' + binaire_code[:3] + '10' + binaire_code[3:9] + '10' + binaire_code[9:15] + '10' + binaire_code[15:]
```

***Q4.*** Écrire une fonction `str_to_utf8`, qui prend en paramètre une chaine de caractère et renvoie la représentation binaire UTF-8, en hexadécimal, de l'ensemble des caractères de la chaine. Exécutez votre fonction sur la chaine `Rien ne sert de courir ; il faut partir à point. Le Lièvre et la Tortue en sont un témoignage.` Comparez le résultat obtenu avec celui de l'observation 12.

```python
def str_to_utf8(chaine):
  l = []
  for caractere in chaine:
    l.append(hexadecimal(binaire_utf8(ord(caractere))))
  return l
```



**Réflexion Finale** :

1. Pourquoi est-il important de connaître l'encodage utilisé pour un texte ?
2. Quels problèmes peut-on rencontrer si on utilise le mauvais encodage pour lire un texte ?
3. Comment l'UTF-8 a-t-il aidé à résoudre certains de ces problèmes ?



--------------

Auteurs : Florian Mathieu - Philippe Boddaert

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
