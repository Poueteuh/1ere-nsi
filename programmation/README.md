# Programmation Python

## Présentation

Ce chapitre a pour objectif de répondre aux questions suivantes : 

- Qu'est-ce que __programmer__ ?
- Qu'est que __Python__ ?
- Comment programmer en Python ?

## Progression

| Section                                            | Description                                        |
| -------------------------------------------------- | -------------------------------------------------- |
| [Généralités sur la programmation](GENERALITES.md) | Introduction à la programmation                    |
| [Python](NOTEBOOK.ipynb)                           | Introduction à Python                              |
| [Variables](chapitre_1/README.md)                  | Exploration et utilisation des variables en Python |
| [Structures conditionnelles](chapitre_2/README.md) | Apprendre les if, elif et else                     |
| [Boucles](chapitre_3/README.md)                    | Comprendre les boucles while et for                |
| [Fonctions](chapitre_4/README.md)                  | Création et utilisation des fonctions en Python    |

## Attendus du Programme

| Contenus                                                     | Capacités attendues                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Constructions élémentaires | Mettre en évidence un corpus de constructions élémentaires. Séquences, affectation, conditionnelles, boucles bornées, boucles non bornées, appels de fonction. |
| Diversité et unité des langages de programmation | Repérer, dans un nouveau langage de programmation, les traits communs et les traits particuliers à ce langage. |
| Spécification | Prototyper une fonction. Décrire les préconditions sur les arguments. Décrire des postconditions sur les résultats.|
| Mise au point de programmes | Utiliser des jeux de tests. |
| Utilisation de bibliothèques | Utiliser la documentation d’une bibliothèque. |
