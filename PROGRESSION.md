| Séquence | Contenu                                                      | Projets associés           |
| -------- | ------------------------------------------------------------ | -------------------------- |
| 1 - a    | Introduction NSI, Découverte du markdown, jupyter notebook   | -                          |
| 1 - b    | Introduction à la programmation et révisions python - variables, instructions conditionnelles, boucles, fonctions | -                          |
| 2 - a    | Numération - Représentation des données : types et valeurs de base | Calculatrice               |
| 2 - b    | Représentation des données, types construits : listes, tuples, dictionnaires, données en table, structures imbriquées et compréhensions | Projet Pokedex, Zoo        |
| 3 - a    | Interactions entre l’homme et la machine sur le Web : html, css, javascript | Projet site web, projet JS |
| 3 - b    | Constructions élémentaires, spécifications, mise au point de programmes, utilisation de bibliothèques | Mise au point de fonctions |
| 4 - a    | Architecture Informatique : historique de l'informatique, modèle Von Neumann, os... | Linux, M999                |
| 4 - b    | Algorithmes : parcours séquentiel d'un tableau, algorithmes de tri, dichotomie... | Problème du crêpier        |
| 5 - a    | Algorithmes avancés : algorithmes gloutons, knn...           | Pokedex avancé, Choipeaux  |
| 5 - b    | Réseau : tcp / ip, dns...                                    | Projet Filius              |
| 6        | Projets finaux                                               | Au choix                   |



