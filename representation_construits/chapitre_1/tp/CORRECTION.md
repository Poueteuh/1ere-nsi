```python
def mastermind(combinaison,proposition):
    """
    Compare la proposition du joueur avec la combinaison secrète et renvoie les indices correspondants.
    
    :param combinaison : liste contenant la combinaison secrète
    :param proposition : liste contenant la proposition du joueur
    :return indices :  liste contenant les indices pour la proposition du joueur
    
    """
    indices = []
    combinaison_temp = combinaison_secrete.copy()
    
    # Vérification des couleurs bien placées
    for i in range(len(proposition)):
        if proposition[i] == combinaison_secrete[i]:
            indices.append('X')
            combinaison_temp[i] = None  # Marquer la couleur comme déjà utilisée dans la combinaison secrète
    
    # Vérification des couleurs présentes mais mal placées
    for i in range(len(proposition)):
        if proposition[i] in combinaison_temp:
            indices.append('O')
            combinaison_temp[combinaison_temp.index(proposition[i])] = None  # Marquer la couleur comme déjà utilisée
    
    return indices

```







```python
def mastermind(combinaison, proposition):
    """
    Compare la proposition du joueur avec la combinaison secrète et renvoie les indices correspondants.

    :param combinaison : liste contenant la combinaison secrète
    :param proposition : liste contenant la proposition du joueur
    :return indices :  liste contenant les indices pour la proposition du joueur
    """
    indices = []
    combinaison_copie = combinaison.copy()  # Pour garder la combinaison originale intacte

    # Étape 1: Vérifier les correspondances exactes
    for couleur_proposee in proposition:
        if couleur_proposee in combinaison_copie:
            if couleur_proposee == combinaison_copie[0]:  # Compare avec le premier élément
                indices.append('N')  # Bonne couleur, bonne position
                combinaison_copie[0] = None  # Marquer comme traité
            else:
                indices.append('B')  # Bonne couleur, mauvaise position
                combinaison_copie.remove(couleur_proposee)  # Retirer l'élément traité

    return indices

# Générer une combinaison secrète aléatoire
import random
couleurs = ['Rouge', 'Vert', 'Bleu', 'Jaune', 'Orange', 'Violet']
combinaison_secrete = random.sample(couleurs, 4)

# Boucle de jeu
while True:
    proposition_joueur = input("Entrez votre proposition (séparée par des espaces) : ").split()
    indices = mastermind(combinaison_secrete, proposition_joueur)
    print("Indices :", indices)
    if indices == ['N', 'N', 'N', 'N']:
        print("Félicitations, vous avez trouvé la combinaison secrète !")
        break

```

