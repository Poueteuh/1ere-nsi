## Représentation des données : tuples, listes et itérations

-------------

> ***Contenue*** : Notions introduites : le type conteneur, avec les listes, les tuples, parcours de séquences. 
> ***Compétences*** : Comprendre la différence entre une liste python et un tuple. Maîtriser la création de conteneurs sur Python.

## Le programme

-----------

![bo_tuple](../assets/bo_tuple.png)

![bo_tab](../assets/bo_tab.png)

## Apport de connaissances

----------

Les types *conteneurs* comme les tuples ou les listes permettent de stocker des objets, et proposent des méthodes d'accès, de modification et d'itération sur ces mêmes objets.

Ici nous allons voir trois types de conteneurs en particulier : les tuples, les listes, et enfin les dictionnaires. En apprenant à créer et à manipuler ces types d'objets, cela nous permettra de travailler sur un grand nombre de données de manière rapide et efficace.

--------

### Les listes

Pour le moment, nous n'avons appris à travailler qu'avec un nombre limité de variables. Cependant, il peut arriver que vous deviez lire, stocker, traiter et enfin imprimer des dizaines, voire des centaines ou des milliers de valeurs de données. Comment accéder efficacement à ces données ? 

Par exemple, si l'on décide de de manipuler une demi-douzaine de variables, doit on les déclarer comme ceci ?  

```python
var1 = int(input())
var2 = int(input())
var3 = int(input())
var4 = int(input())
var5 = int(input())
var6 = int(input())
```

Nous allons voir que la structure de données appelée liste va nous permettre de déclarer une variable à valeurs multiples.

-----------------

### Créer une liste

- Vide :

```python
liste = [ ] 
```

- On peut les créer par ***extension*** (comprendre *une liste déclaré littéralement*) comme ceci :

```python
liste = [3, 8, 5, 4]
```

- Mais aussi par ***compréhension*** (itération, que l'on verra plus loin)

On peut tester ça dans la console python 

```python
liste = [2, 3, 5, 7 ]
type (liste)
```

Que retournent ces lignes python ?

On peut afficher la liste de la même manière qu'une variable classique :  avec la commande *print* 

```python
print(liste)
```

--------------

### Accéder aux élèments d'une liste

Les élèments d'une liste peuvent être comparés à des élèments d'une séquence : chaque élèment a une position précise dans le conteneur, comme nous l'avons vu pour les chaînes de caractères (*str*).

On appelle cette position *indice*, c'est cet indice qui nous permet d'accéder à un élèment en particulier.

Le premier élèment d'une liste a pour indice **0**.

Tout comme avec les chaînes de caractères, un indice négatif permet d'accéder l'élèment en partant de la fin.

Pour connaître le nombre total d'élèments dans une liste, on utilise la fonction ***len***

```python
l_1 = [2,4,6,8]
len (l_1)
```

On peut désigner une **tranche** en indiquant l'indice du premier élèment, ainsi que celui du dernier ***non inclus*** :

```py
l_1[1:3]
```

Attention : Un indice qui dépasse la valeur *longueur de la liste - 1* provoquera une erreur dans votre code

----------

### Parcours d'une séquence

Comme indiqué plus haut, il est possible de créer et de parcourir une liste par une méthode dite *de compréhension* ou *itération*.

Celà signifie qu'on construit cette liste à partir d'élèments itérables : que l'on peut répétèr. Par exemple :

```pyth
liste = [x for x in range(5)]
print(liste)
```

Que vous affiche la console ?

L'intérêt des listes par compréhension, c'est que l'on peut utiliser les structures conditionnelles, à l'aide de boucles...

On peut également initialiser une liste à partir d'une autre :

```pytho
liste_1 = [1, 2, 3, 4]
liste_2 = [2 * x for x in liste_1]
print(liste_2)
```

Une fois que votre conteneur est crée, on peut le parcourir :

```pyth
for elt in liste_2:
	print (elt, end='')
```

Ici, on va afficher chaque élèment de liste_2.

On peut également utiliser une boucle en itérant l'indice i des élèments :

```python
for i in range (len(liste_2)):
	print(liste_2[i], end='')
```

Ici, pour i compris entre 0 et longueur de liste_2, on va affichier l'élèment correspondant à l'indice valant i.

 À faire : 

- créer une liste par extension, puis par itération
- afficher les élèments de cette liste

-----------

### Propriétés

Une structure comme une liste peut être modifiable : on parle d'un objet *mutable*

- On peut donc supprimer un élèment de la séquence avec le mot clé ***del***

  ```python
  toto = [1,2,3,4,5]
  del toto[0]
  print(toto)
  [2,3,4,5]
  ```

  Il faut noter qu'en supprimant l'élèment situé à l'indice 0, j'ai fait changer tous les indices des élèments de la liste !

- On peut également supprimer la première occurence d'un élement à la valeur spécifiée avec le mot clé **remove** 

```python
liste = [1, 2, 3, 2, 4, 5]
liste.remove(2)
print(liste)
[1,3,2,4,5]
```

- Pour en terminer avec la suppression d'élément, le mot clé ***pop*** permet de supprimer et de renvoyer l'élément à une position spécifique. Par défaut, il supprime et renvoie le dernier élément de la liste

```python
liste = [1, 2, 3, 4, 5]
dernier = liste.pop()
print(dernier)
5
print(liste)
[1, 2, 3, 4]
```



- On peut ajouter un élèment avec le mot clé ***append***  : 

  ```python
  liste = [1,2,3,4,5]
  liste.append(6)
  print (liste)
  [1,2,3,4,5,6]
  ```

  - L'élèment ajouté se positionne donc à la *fin* de la liste

    

- On peut également insérer un élément à une position spécifique dans la liste, avec ***insert*** :

  ```python
  liste = [1, 2, 3, 4, 5]
  liste.insert(2, 'trois')
  print(liste)
  [1, 2, 'trois', 3, 4, 5]
  ```

  

  

  

- Pour modifier un élèment en particulier, on lui affecte une nouvelle valeur :

```python
t = [1,2,3,4,5]
t[1] = 8
print (t)
[1,8,3,4,5]
```

- Il est préférable de ne stocker que des données de même type dans une liste.



- Enfin, il est possible de trier les éléments d'une liste en place, c'est-à-dire qu'elle modifie la liste elle-même, avec le mot clé ***sort***

```python
liste = [3, 1, 4, 1, 5, 9]
liste.sort()
print(liste)
[1, 1, 3, 4, 5, 9]
```



- Une liste peut-être passée comme paramètre d'une fonction

--------



## Les tuples (ou p-uplets)

 À la différence des listes, les tuples ne sont ***pas mutables***. On ne peut donc plus les modifier une fois créés, leur taille est fixe.

Mais ils peuvent néanmoins contenir des données de différents types.

Pour initialiser un tuple, on utilise cette syntaxe :

```pyth
tuple = (1, 3)
print (tuple)
(1, 3)
```

Ici, de la même manière qu'avec les listes, on pourra accéder à un élèment en particulier via son indice

```python
tuple = (1, 3, 5, 7, 9)
tuple[-1]
9
```

De plus, on peut tout à fait utiliser l'itération pour construire ou parcourir une structure tuple.

Enfin, il est courant, dans une fonction, de renvoyer plusieurs valeurs. Par exemple, via ce type de syntaxe :

```pyth
return a, b
```

En réalité, cette syntaxe signifie : return (a, b) : on a donc un tuple composé des variables a et b qui est renvoyé.



- Attention : ce n'est pas parce que le tuple n'est pas mutable que ce qu'il contient ne l'est pas ! Exemple, un tuple peut contenir une liste : 

```python
t = (1, 2, ['a', 'b'])
print(t)
(1, 2, ['a', 'b'])

t[2].append('c')
print(t)
(1, 2, ['a', 'b', 'c'])

```

- Le tuple lui-même reste le même (il contient toujours trois éléments et les deux premiers sont toujours 1 et 2), mais la liste à l'intérieur du tuple a été modifiée. 

- C'est pourquoi on dit que les tuples peuvent "contenir" des éléments mutables. 

------------



## Liste vs Tuple

Voici les différences notables finalement :

- **Mutabilité** : La différence la plus marquante est que les listes sont mutables (c'est-à-dire qu'elles peuvent être modifiées après leur création) tandis que les tuples sont immuables (c'est-à-dire qu'ils ne peuvent pas être modifiés après leur création).

- **Utilisation** : En général, les listes sont utilisées lorsque vous avez une collection d'éléments qui peut changer dans le temps (par exemple, une liste de tous vos contacts, de vos pokemon préférés). Les tuples sont utilisés lorsque vous avez une collection d'éléments qui ne changera pas (par exemple, votre date de naissance, ou un ensemble de coordonnées géographiques).

- **Performance** : Les tuples peuvent être plus rapides à parcourir que les listes et occupent moins d'espace mémoire, car ils sont immuables. Cela peut avoir un impact significatif dans les programmes où la performance est critique.

  

----------

### À retenir:

***Pour les listes et tuples :***

| Opération  | Résultat                                                     |
| ---------- | ------------------------------------------------------------ |
| x in t     | Renvoie True si un élément de t est égal à x                 |
| x not in t | Renvoie True si aucun élément de t n'est égal à x, False sinon |
| len(t)     | Renvoie le nombre d'éléments de t                            |
| t == t1    | Renvoie True si t et t1 sont de même type, ont la même longueur, et ont des éléments égaux 2 à 2 |

| Opération  | Résultat                                                     |
| ---------- | ------------------------------------------------------------ |
| t[i]       | Renvoie l'élément d'indice i de t                            |
| t[i:j]     | Renvoie une partie de t de l'indice i à j non inclus         |
| t.index(x) | Renvoie l'indice de la première apparition de x dans t       |
| t.count(x) | Renvoie le nombre d'apparitions de x dans t                  |
| s + t      | Renvoie une nouvelle séquence qui correspond à la concaténation de s et de t (seulement si s et t ont le même type) |

***Uniquement pour les listes :*** 

| Opération      | Résultat                                                     |
| -------------- | ------------------------------------------------------------ |
| t.append(x)    | Ajoute l'élément x à la fin de la liste t                    |
| t[i] = x       | Modifie la liste et affecte la valeur x à la case d'indice i |
| t.insert(i, x) | Insère l'élément x  à la position de l'indice i              |
| t.remove(x)    | Supprime le premier élément dont la valeur est égale à x     |
| t.pop(i)       | Enlève de la liste l'élément à la position i et renvoie sa valeur |

-----------

Auteur : Florian Mathieu

Licence CC BY NC

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a> <br />Ce cours est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.
