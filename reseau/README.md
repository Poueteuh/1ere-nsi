> Dans le monde ultra connecté qu'est le notre, Internet et les réseaux tiennent une place primordiale.
>
> Plus de trente (30) milliards d'appareils sont connectés quotidiennement à un réseau. Comment cela est-il possible ?
>
> Qu'est ce qui nous attend de ce côté dans le futur ?

### Le programme

![bo](assets/bo.png)

---------

### Définitions

**Définition 1** : Un réseau est un ensemble d’entités interconnectées ou maintenues en
liaison pour réaliser l’échange ou la circulation de biens ou de choses.

**Définition 2** : Un réseau est un ensemble de nœuds (ou pôles) reliés entre eux par des
liens (ou canaux).

En informatique, un réseau informatique est un ensemble d'élèments matériels et logiciels reliés ensemble afin de transporter des informations. Il permet de mettre en oeuvre des services.

- Quels types de services ?

- Avec quels types de matériels ?

  



